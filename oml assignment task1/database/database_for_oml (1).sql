-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2021 at 10:39 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `database_for_oml`
--

-- --------------------------------------------------------

--
-- Table structure for table `oml_problem_topics`
--

CREATE TABLE `oml_problem_topics` (
  `id` int(10) NOT NULL,
  `topic` varchar(100) NOT NULL,
  `created-at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `oml_problem_topics`
--

INSERT INTO `oml_problem_topics` (`id`, `topic`, `created-at`) VALUES
(1, 'depression', '2021-06-18 05:29:09'),
(2, 'anxiety', '2021-06-18 05:29:09'),
(3, 'financial stress', '2021-06-18 05:29:09'),
(4, 'family stress', '2021-06-18 05:29:09'),
(5, 'sexual wellness', '2021-06-18 05:29:09'),
(6, 'alcohol abuse', '2021-06-18 05:29:09'),
(7, 'substance abuse', '2021-06-18 05:29:09'),
(8, 'bipolar disorder', '2021-06-18 05:29:09');

-- --------------------------------------------------------

--
-- Table structure for table `oml_user`
--

CREATE TABLE `oml_user` (
  `id` int(10) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `profile_photo` varchar(200) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `about_me` varchar(250) NOT NULL,
  `topics` varchar(252) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `oml_user`
--

INSERT INTO `oml_user` (`id`, `name`, `email`, `password`, `profile_photo`, `dob`, `gender`, `about_me`, `topics`, `created_at`, `updated_at`) VALUES
(8, 'rohindra', '', '', 'rohindra photo.JPG', '1994-04-12', 'male', 'just test', '1,9,3,5,7', '2021-06-18 06:59:08', '2021-06-18 06:59:08'),
(10, 'rohindra', 'rohindra.y@engineerbabu.in', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'rohindra photo.JPG', '1994-04-12', 'male', 'just test', '1,2,3', '2021-06-18 07:26:43', '2021-06-18 07:26:43'),
(11, 'rohindra', 'rohindra3232.y@engineerbabu.in', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'rohindra photo.JPG', '1994-04-12', 'male', 'just test', '1,2,3,4', '2021-06-18 08:27:21', '2021-06-18 08:27:21'),
(12, 'rohindra', 'rohindra32.y@engineerbabu.in', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'rohindra photo.JPG', '1994-04-12', 'male', 'just test', '3,4,7', '2021-06-18 08:27:43', '2021-06-18 08:27:43'),
(13, 'rohindra', 'rohindra2.y@engineerbabu.in', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'rohindra photo.JPG', '1994-04-12', 'male', 'just test', '2,3,4,7', '2021-06-18 08:28:34', '2021-06-18 08:28:34'),
(14, 'rohindraTest', 'rohindra24343.y@engineerbabu.in', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'rohindra photo.JPG', '2000-04-12', 'male', 'just test ok test it', '1,2,3,4,7', '2021-06-18 09:51:01', '2021-06-18 09:51:01'),
(15, 'rohindraTest', 'rohindra243.y@engineerbabu.in', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'test.jpg', '2000-04-12', 'male', 'just test ok test it', '1,2', '2021-06-21 05:44:28', '2021-06-21 05:44:28'),
(16, 'rohindraTest', 'rohindra2645643.y@engineerbabu.in', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'test.jpg', '2000-04-12', 'male', 'just test ok test it', '1,2,3,4', '2021-06-21 07:19:19', '2021-06-21 07:19:19'),
(17, 'rohindraTest', 'rohindra1111.y@engineerbabu.in', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'test.jpg', '2000-04-12', 'male', 'just test ok test it', '1,2,3,4,5', '2021-06-21 07:41:54', '2021-06-21 07:41:54'),
(18, 'rohindraTest', 'rohindra1211.y@engineerbabu.in', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'test.jpg', '2005-04-12', 'male', 'just test ok test it', '1,2,3,4,5', '2021-06-21 07:43:28', '2021-06-21 07:43:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oml_problem_topics`
--
ALTER TABLE `oml_problem_topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oml_user`
--
ALTER TABLE `oml_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oml_problem_topics`
--
ALTER TABLE `oml_problem_topics`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oml_user`
--
ALTER TABLE `oml_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
