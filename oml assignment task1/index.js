const path = require('path')
var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var fileUpload = require('express-fileupload');
var port = process.env.PORT || 3000;


app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileUpload());


app.use(require('./routes/route'));
app.listen(port, function() {
    console.log(`server listening on Port No. ${port}`);
});