const express = require("express");
const router = express.Router();
const checkAuth = require("../middleware/check-auth");
const user = require("../controller/user");

router.post("/api/userSignup", user.userSignup);
router.post("/api/authenticate", user.authenticate);
router.post("/api/userprofile", checkAuth, user.userprofile);
router.get("/api/getTopics", user.getTopics);
router.get("/api/getUserList", user.getUserList);
router.post("/api/getUserListAccordingToCondition", user.getUserListAccordingToCondition);



module.exports = router;
