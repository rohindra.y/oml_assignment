const jwt = require('jsonwebtoken');
var key = require('../config');
const JwtKey = key.jwtKey;
module.exports = (req, res, next) => {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
        return res.status(401).json({
            message: "Not Authenticated"
        });
    }
    const token = authHeader.split(" ")[1];
    let decoded;
    try {
        decoded = jwt.verify(token, JwtKey);
        req.userData = decoded;
        next();
    } catch (error) {
        return res.status(401).json({
            message: error
        });
    }
    if (!decoded) {
        return res.status(401).json({
            message: "Authentication failed!"
        });
    }
}