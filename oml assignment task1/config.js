var mysql      =  require('mysql');
var connection = mysql.createConnection({
    host            : 'localhost',
    user            : 'root',
    password        : '',
    database        : 'database_for_oml',
    multipleStatements:true
    });
    connection.connect(function(err){
        if(!err) {   
            console.log("Database is connected");
        } else {
            console.log("Error while connecting with database");
            console.log(err);
        }
    });

module.exports = connection;
module.exports.jwtKey = "omlsecretkeyforjsonwebtoken";