var connection = require("../config");
var sha1 = require("sha1");
const jwt = require("jsonwebtoken");
var key = require('../config');
const JwtKey = key.jwtKey;

module.exports.authenticate = function (req, res) {
    var email = req.body.email;
    var password = req.body.password;
    if (email == "" || email == undefined) {
        res.json({
            message: "please insert email",
        });
        return false;
    }
    if (password == "" || password == undefined) {
        res.json({
            message: "please insert password",
        });
        return false;
    }
    connection.query("SELECT * FROM oml_user WHERE email = ?", [email], function (error, results) {
        if (error) {
            res.status(500).json({
                status: false,
                message: "there are some error with query",
            });
        } else {
            if (sha1(password) == results[0].password) {
                const token = jwt.sign(
                    {
                        email: results[0].email,
                        userId: results[0].id,
                    },
                    JwtKey,
                    {
                        expiresIn: "2h",
                    }
                );
                res.status(200).json({
                    status: true,
                    message: "successfully authenticated",
                    data: results,
                    token: token,
                });

            } else {
                res.json({
                    status: false,
                    message: "Email and password does not match",
                });
            }
        }
    }
    );
};
module.exports.getTopics = function (req, res) {

    connection.query("SELECT id, topic FROM `oml_problem_topics`", function (error, results) {
        console.log(error)
        if (error) {
            res.json({
                status: false,
                message: "Something went wrong",
                data: results,
            });
        } else {
            res.json({
                status: true,
                message: "data fetch successfully",
                data: results,
            });
        }
    }
    );
};
module.exports.userSignup = function (req, res) {

    if (req.body.name == "" || req.body.name == undefined) {
        res.json({
            message: "please insert user name",
        });
        return false;
    }
    if (req.body.email == "" || req.body.email == undefined) {
        res.json({
            message: "please insert user email",
        });
        return false;
    }
    if (req.body.password == "" || req.body.password == undefined) {
        res.json({
            message: "please insert password",
        });
        return false;
    }
    if (req.body.dob == "" || req.body.dob == undefined) {
        res.json({
            message: "please insert user dob",
        });
        return false;
    }
    if (req.body.gender == "" || req.body.gender == undefined) {
        res.json({
            message: "please insert user gender",
        });
        return false;
    }
    if (req.body.about_me == "" || req.body.about_me == undefined) {
        res.json({
            message: "please insert about_me",
        });
        return false;
    }
    if (req.body.topics == "" || req.body.topics == undefined) {
        res.json({
            message: "please insert topics",
        });
        return false;
    }
    if (!req.files) {
        res.json({
            message: "please upload file",
        });
        return false;
    }

    connection.query("SELECT * FROM oml_user WHERE email = ?", [req.body.email], function (error, findUser) {
        if (!error) {
            if (findUser[0]) {
                res.status(500).json({
                    status: false,
                    message: "Email already exist in database!",
                });
            } else {

                var file = req.files.uploaded_image;
                var img_name = file.name;

                if (file.mimetype == "image/jpeg" || file.mimetype == "image/png" || file.mimetype == "image/gif") {

                    file.mv('public/images/user_profile_image/' + file.name, function (err) {
                        if (err) {
                            res.json({
                                status: false,
                                message: "Something went wrong",
                                data: err,
                            });
                        } else {
                            var sql = "INSERT INTO `oml_user`(`name`, `email`, `password`, `dob`, `gender`,`about_me`, `topics` ,`profile_photo`) VALUES ('" + req.body.name + "', '" + req.body.email + "', '" + sha1(req.body.password) + "','" + req.body.dob + "','" + req.body.gender + "','" + req.body.about_me + "','" + req.body.topics + "','" + img_name + "')";
                            console.log(sql)
                            connection.query(sql, function (err, result) {
                                res.json({
                                    status: true,
                                    message: "User signup successfully",
                                    data: result,
                                });
                            });
                        }
                    });
                } else {
                    res.json({
                        status: false,
                        message: "This format is not allowed , please upload file with '.png','.gif','.jpg'",
                    });
                }
            }
        }
    })

};
module.exports.getUserList = function (req, res) {

    connection.query("SELECT * FROM oml_user", function (error, results) {
        if (error) {
            res.json({
                status: false,
                message: "Something went wrong",
                data: results,
            });
        } else {
            res.json({
                status: true,
                message: "data fetch successfully",
                data: results,
            });
        }
    }
    );
};
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

module.exports.getUserListAccordingToCondition = async function (req, res) {
    const id = req.body.id;
    if (id == "" || id == undefined) {
        res.json({
            message: "please insert user id",
        });
        return false;
    }

    connection.query("SELECT * FROM oml_user WHERE id = ?", [id], function (error, findUser) {
        if (!error) {
            if (findUser[0]) {
                let sql = `SELECT U.name, U.email, U.dob, U.topics FROM oml_user U WHERE  (FLOOR(DATEDIFF(U.dob, '${formatDate(findUser[0].dob)}')/365) <> 6) AND id != ${findUser[0].id}`;
                console.log('sql', sql)
                connection.query(sql, async function (error, results) {
                    if (error) {
                        res.json({
                            status: false,
                            message: "Something went wrong",
                            data: results,
                        });
                    } else {
                        var userTopics = (findUser[0].topics).split(',');
                        console.log(userTopics)

                        function getArraysIntersection(a1, a2) {
                            return a1.filter(function (n) { return a2.indexOf(n) !== -1; });
                        }

                        let finalData = [];
                        results.forEach(element => {
                            console.log(getArraysIntersection(userTopics, (element.topics).split(',')))
                            if ((getArraysIntersection(userTopics, (element.topics).split(','))).length >= 3) {
                                finalData.push(element);
                            }
                        });

                        res.json({
                            status: true,
                            message: "data fetch successfully",
                            data: finalData,
                        });
                    }
                });
            } else {
                res.json({
                    status: false,
                    message: "Something went wrong",
                });
            }
        } else {
            res.json({
                status: false,
                message: "Something went wrong",
            });
        }
    })
};

module.exports.userprofile = function (req, res) {
    const id = req.body.id;
    if (id == "" || id == undefined) {
        res.json({
            message: "please insert user id",
        });
        return false;
    }
    connection.query("SELECT * FROM oml_user WHERE id = ?", [id], function (error, results) {
        if (error) {
            res.json({
                status: false,
                message: "Something went wrong",
                data: results,
            });
        } else {
            res.json({
                status: true,
                message: "data fetch successfully",
                data: results,
            });
        }
    }
    );
};

