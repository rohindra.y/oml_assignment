I have completed task1 and task2 to check task follow these steps

1. Task1 : This task give database design that is on folder oml assignment task2 
           that include png image and pdf for database table and its relationships

2. Task2 : This task say to design API for given conditions.

            To run second task user ready made database that i have given on database folder
            create database and import it.

            then go inside oml assignment task2 and run command npm install to install its dependency
            then run project by a node index or nodemon 
            then it will show port number and database connection message

            then go to postman and import collection by a link and enter https://www.getpostman.com/collections/2b7e41f159c03ae56bd9
            then test given api and check according to given conditions.
        